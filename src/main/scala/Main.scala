/*
 * The main MineSweeper program.
 *
 * The class Main is the toplevel Swing application. It provides the menu and
 * launchs new games.
 */

import swing._
import javax.swing.{UIManager,UnsupportedLookAndFeelException}
import IntField.IntField
import PopupWindow.PopupWindow

/* This object gather every predefined values. */
object Config
{
	val defaultSettings = Game.Settings(h=9, w=9, mines=16)
	val easySettings    = Game.Settings(h=9, w=9, mines=10)
	val mediumSettings  = Game.Settings(h=16, w=16, mines=40)
	val hardSettings    = Game.Settings(h=16, w=16, mines=99)
}

object Main extends SimpleSwingApplication
{
	/* the last game setting used: */
	var settings = Config.defaultSettings
	/* Set the look&feel (before doing anything else). */
	try {
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel")
	}
	catch {
	 case _ : UnsupportedLookAndFeelException =>
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
	}
	/* Enable antialiasing. */
	System.setProperty("awt.useSystemAAFontSettings", "lcd")
	System.setProperty("swing.aatext", "true")
	/* the popup that asks the user for custom game settings: */
	val customGamePopup = new PopupWindow[(Int,Int,Int,String)] {
		title = "Custom grid"
		val hField     = new IntField(3, Config.defaultSettings.h)
		val wField     = new IntField(3, Config.defaultSettings.w)
		val minesField = new IntField(3, Config.defaultSettings.mines)
		val seedField = new TextField
		val okButton     = new Button(Action("OK") {
			popupDone(hField.value, wField.value, minesField.value, seedField.text)
		})
		val cancelButton = new Button(Action("Cancel") {
			popupCancel()
		})
		hField    .action = Action("") {     wField.requestFocus }
		wField    .action = Action("") { minesField.requestFocus }
		minesField.action = Action("") {  seedField.requestFocus }
		seedField .action = okButton.action
		/* Do not allow the user to enter more mines than the number of cells. */
		minesField.check = (value:Int) => {
			val b = value <= hField.value * wField.value
			okButton.enabled = b
			b
		}
		contents = new BoxPanel(Orientation.Vertical) {
			contents += new FlowPanel {
				contents += hField
				contents += new Label("×")
				contents += wField
				contents += new Label(", ")
				contents += minesField
				contents += new Label("mines")
			}
			contents += new BoxPanel(Orientation.Horizontal) {
				contents += new Label("seed:")
				contents += seedField
			}
			contents += new BoxPanel(Orientation.Horizontal) {
				contents += okButton
				contents += Swing.HGlue
				contents += cancelButton
			}
		}
	}
	/* the popup that explains the controls: */
	val helpPopup = new PopupWindow {
		title = "Help — Controls"
		contents = new GridPanel(7, 2) {
			contents += new Label("left click or <Space>")
			contents += new Label("reveal")
			contents += new Label("right click or <Alt>")
			contents += new Label("flag")
			contents += new Label("click or arrow keys")
			contents += new Label("select")
			contents += new FlowPanel {
				contents += new Button("") {
					icon = Game.Resource.imgStatusPlaying
					focusable = false
				}
				contents += new Label(" or <Enter>")
			}
			contents += new Label("defuse")
			contents += new FlowPanel {
				contents += new Button("") {
					icon = Game.Resource.imgStatusMercy
					focusable = false
				}
				contents += new Label("or <Enter>")
			}
			contents += new Label("you did a bad move — cancel it")
			contents += new FlowPanel {
				contents += new Button("") {
					icon = Game.Resource.imgStatusLost
					focusable = false
				}
				contents += new Label("or <Enter>")
			}
			contents += new Label("you lost — play again")
			contents += new FlowPanel {
				contents += new Button("") {
					icon = Game.Resource.imgStatusWon
					focusable = false
				}
				contents += new Label("or <Enter>")
			}
			contents += new Label("you won — play again")
		}
	}
	/* the window menu: */
	val menu = new MenuBar {
		contents += new Menu("New") {
			mnemonic = event.Key.N
			contents += new MenuItem(Action("Default ("+Config.defaultSettings+")") {
				settings = Config.defaultSettings
				newGame()
			}) { mnemonic = event.Key.D }
			contents += new Separator
			contents += new MenuItem(Action("Easy ("+Config.easySettings+")") {
				settings = Config.easySettings
				newGame()
			}) { mnemonic = event.Key.E }
			contents += new MenuItem(Action("Medium ("+Config.mediumSettings+")") {
				settings = Config.mediumSettings
				newGame()
			}) { mnemonic = event.Key.M }
			contents += new MenuItem(Action("Hard ("+Config.hardSettings+")") {
				settings = Config.hardSettings
				newGame()
			}) { mnemonic = event.Key.H }
			contents += new Separator
			contents += new MenuItem(Action("Custom") {
				customGamePopup.show() match {
				  case None            => ()
				  case Some((h,w,m,s)) => settings = Game.Settings(h,w,m); newGame(s)
				}
			}) { mnemonic = event.Key.C }
		}
		contents += new MenuItem(Action("Again") {
			newGame()
		}) { mnemonic = event.Key.A }
		contents += new MenuItem(Action("Help") {
			helpPopup.show()
		}) { mnemonic = event.Key.H }
		contents += new MenuItem(Action("Quit") {
			sys.exit(0)
		}) {  mnemonic = event.Key.Q }
	}
	/* the toplevel component: */
	val ui = new MainFrame {
		title = "MineSweeper MegaSwag Attack"
		peer.setLocationRelativeTo(null)
	}
	/* Launch a game on startup. */
	newGame()
	/* Handle new game requests fired by the ‘GameUI’ component. */
	reactions += {
	  case Game.NewGameRequest() => newGame()
	}
	def top = ui
	/* This method launchs a new game with the settings stored in ‘settings’ and
	   optionnaly a seed string. */
	def newGame(seedStr:String = "") : Unit = {
		val seed = if (seedStr == "") 0 else seedStr.hashCode
		val game = new Game.GameUI(new Game.Game(settings, seed))
		ui.contents = new BorderPanel {
			layout(menu) = BorderPanel.Position.North
			layout(game) = BorderPanel.Position.Center
		}
		listenTo(game)
		game.gridUI.requestFocus()
	}
}
