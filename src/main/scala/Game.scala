/*
 * The core MineSweeper game.
 *
 * This file defines a class ‘Game’ that implements the game itself without
 * bothering with displaying, and a class ‘GameUI’ that implements the graphical
 * interface of that game.
 */
package Game

/* The variant type ‘Cell’ is the type of MineSweeper’s cells. */
abstract class Cell
{
	var revealed = false
	var flag     = false
}
case class Empty(count:Int) extends Cell
case class Mine()           extends Cell

/* Represents the status of a game. */
object Status extends Enumeration
{
	val Playing, Mercy, Lost, Won = Value
}

/* Stores the parameters for instanciating a new game. It is useful as it can
 * check their validity and enable easy switching between several game profiles. */
case class Settings(h:Int, w:Int, mines:Int)
{
	assert(mines <= h*w)
	override def toString =
		h + "×" + w + ", " + mines + " mines"
}

class Game(settings:Settings, seed:Int = 0)
{
	/* accessors for the settings: */
	val h = settings.h
	val w = settings.w
	val mines = settings.mines
	/* the game grid: */
	val grid = Array.tabulate[Cell](h, w)((y,x) => Empty(0))
	/* number of flags used: */
	var flags = 0
	/* game status: */
	var status = Status.Playing
	/* coordinates of the last revealed cell (useful for the ‘mercy’ mode): */
	var (lastY,lastX) = (0,0)
	/* Randomly generate the grid. */
	private val rand = if (seed == 0) new util.Random() else new util.Random(seed)
	var minesPos : List[(Int,Int)] = Nil
	for (_ <- 1 to mines) {
		var (y,x) = (0,0)
		do {
			y = rand.nextInt(h)
			x = rand.nextInt(w)
		} while (grid(y)(x) == Mine())
		minesPos = (y,x) :: minesPos
		grid(y)(x) = Mine()
		incr(y-1, x-1)
		incr(y-1, x  )
		incr(y-1, x+1)
		incr(y  , x-1)
		incr(y  , x+1)
		incr(y+1, x-1)
		incr(y+1, x  )
		incr(y+1, x+1)
	}
	private def incr (y:Int, x:Int) =
		if (0 <= y && y < h && 0 <= x && x < w)
			grid(y)(x) match {
			  case Empty(n) => grid(y)(x) = Empty(n+1)
			  case _        => ()
			}
	/* Tests for the game status. */
	def isPlaying =
		status == Status.Playing
	def hasEnded =
		status == Status.Lost || status == Status.Won
	/* The available game actions. */
	def mark (y:Int, x:Int) = if (isPlaying)
		if (!grid(y)(x).revealed)
			if (grid(y)(x).flag) {
				grid(y)(x).flag = false
				flags -= 1
			}
			else if (flags < mines) {
				grid(y)(x).flag = true
				flags += 1
			}
	def reveal (y:Int, x:Int) = if (isPlaying)
		if (!grid(y)(x).flag) {
			revealZone(y, x)
			if (grid(y)(x) == Mine()) {
				status = Status.Mercy
			}
		}
	private def revealZone (y0:Int, x0:Int) = {
		val stack = new collection.mutable.Stack[(Int,Int)]
		def explore (y:Int, x:Int) =
			if (0 <= y && y < h && 0 <= x && x < w && !grid(y)(x).revealed) {
				if (grid(y)(x).flag)
					mark(y, x)
				grid(y)(x).revealed = true
				lastY = y
				lastX = x
				if (grid(y)(x) == Empty(0))
					stack.push((y,x))
			}
		explore(y0, x0)
		while (!stack.isEmpty) {
			val (y,x) = stack.pop()
			explore(y-1, x-1)
			explore(y-1, x  )
			explore(y-1, x+1)
			explore(y  , x-1)
			explore(y  , x+1)
			explore(y+1, x-1)
			explore(y+1, x  )
			explore(y+1, x+1)
		}
	}
	def cancelLastRevealed () = if (status == Status.Mercy) {
		grid(lastY)(lastX).revealed = false
		status = Status.Playing
	}
	def defuse () = if (isPlaying)
		if (minesPos forall {case (y,x) => grid(y)(x).flag})
			status = Status.Won
		else
			status = Status.Lost
	def restart () = if (!hasEnded) {
		for (y <- 0 until h; x <- 0 until w) {
			grid(y)(x).flag = false
			grid(y)(x).revealed = false
		}
		flags = 0
		status = Status.Playing
	}
}

import swing._
import javax.swing.ImageIcon
import java.awt.{Color,Font}
import java.io.FileNotFoundException
import GridUI._
import Timer.Timer

/* This object gather every useful resource used by the game interface, and
 * also defines some handy methods. */
object Resource
{
	def cellComponent (c:Cell, gameHasEnded:Boolean = false) =
		new Label {
			icon = cellImage(c, gameHasEnded)
			background = cellBackground(c, gameHasEnded)
			opaque = true
		}
	def cellImage (c:Cell, gameHasEnded:Boolean) =
		if (c.revealed || gameHasEnded)
			c match {
			  case Empty(n) => imgNumber(n)
			  case Mine()   => imgMine
			}
		else if (c.flag)
			imgFlag
		else
			imgHidden
	def cellBackground(c:Cell, gameHasEnded:Boolean) =
		if (c == Mine() && c.revealed)
				colorRevealedMine
		else if (c == Mine() && gameHasEnded) {
			if (c.flag)
				colorRightFlag
			else
				colorMissedMine
		}
		else if (c.flag && gameHasEnded)
			colorWrongFlag
		else if (c.revealed)
			colorRevealed
		else
			colorHidden
	private def resURL (relPath:String) = {
		getClass.getResource(relPath) match {
		  case null => throw new FileNotFoundException(relPath)
		  case url  => url
		}
	}
	private def img (path:String) =
		new ImageIcon(resURL(path))
	private def font (path:String) =
		Font.createFont(Font.TRUETYPE_FONT, resURL(path).openStream())
	val imgStatusPlaying = img("/status_playing.png")
	val imgStatusMercy   = img("/status_mercy.png")
	val imgStatusLost    = img("/status_lost.png")
	val imgStatusWon     = img("/status_won.png")
	//val cellSize : Int = 40
	val imgHidden = img("/cell_hidden.png")
	val imgFlag   = img("/cell_flag.png")
	val imgMine   = img("/cell_mine.png")
	val imgNumber = Array.tabulate(9)(i => img("/cell_empty_%d.png" format i))
	val colorHidden       = Color.gray
	val colorRevealed     = Color.white
	val colorRightFlag    = Color.green
	val colorWrongFlag    = Color.yellow
	val colorMissedMine   = Color.orange
	val colorRevealedMine = Color.red
	val fontTimer    = font("/LiquidCrystal.otf").deriveFont(20f)
}

/* This event is thrown when the user asked for a new game from the current game
 * interface (as the ‘GameUI’ object cannot replace itself). */
case class NewGameRequest() extends event.Event

class GameUI(game:Game) extends GridUI(game.h, game.w)
{
	/* The various elements composing the bar above the grid: flag and mine
	 * counters, timer, big “multi”-button (the one with smileys) and restart
	 * button. */
	val flagsCounter = new Label
	val minesCounter = new Label
	val counters = new BoxPanel(Orientation.Vertical) {
		contents += flagsCounter
		contents += minesCounter
	}
	val multiButton = new Button(Action("") {
		multiButtonDo()
		//gridUI.requestFocus()
	})
	val restartButton = new Button(Action("Restart") {
		restart()
		//gridUI.requestFocus()
	})
	var t0 = 0L
	val timerLabel = new Label {
		font = Resource.fontTimer
		border = new javax.swing.border.EmptyBorder(0, 1, 0, 1)
	}
	val timer = Timer(1000) {
		val s = (System.currentTimeMillis - t0) / 1000
		timerLabel.text = "%02d:%02d".format(s/60, s%60)
	}
	val gameControls = new BoxPanel(Orientation.Horizontal) {
		contents += counters
		contents += Swing.HGlue
		contents += multiButton
		contents += Swing.HGlue
		contents += new BoxPanel(Orientation.Vertical) {
			contents += restartButton
			contents += timerLabel
			contents += new BoxPanel(Orientation.Horizontal) {
				contents += restartButton
			}
			contents += new BoxPanel(Orientation.Horizontal) {
				contents += timerLabel
			}
		}
	}
	updateStatus()
	initTimer()
	/* Put those elements above the grid. */
	layout(gameControls) = BorderPanel.Position.North
	/* Disable focusing the buttons so that the grid cannot lose focus. */
	multiButton .focusable = false
	restartButton.focusable = false
	/* Start from the center of the grid (better-looking). */
	select(h/2, w/2)
	/* Handle the events and map them to game actions. */
	reactions += {
	  case event.KeyPressed(_, event.Key.Enter, _,_) =>
		multiButtonDo()
	  case event.KeyPressed(_, event.Key.Space, _,_) =>
		reveal(ysel, xsel)
	  case event.KeyPressed(_, event.Key.Alt, _,_) =>
		mark(ysel, xsel)
	  case e @ event.MouseClicked(CellUI(y,x), _,_,_,_) if e.peer.getButton == 1 =>
		reveal(y, x)
	  case e @ event.MouseClicked(CellUI(y,x), _,_,_,_) if e.peer.getButton == 3 =>
		mark(y, x)
	}
	/* Provide the actual display of cells. */
	def getCellComponent (y:Int, x:Int) =
		Resource.cellComponent(game.grid(y)(x), game.hasEnded)
	/* These methods bind user events received by the ‘GameUI’ object to game
	   actions performed by the ‘Game’ object. */
	def reveal (y:Int, x:Int) =
		if (game.grid(y)(x).flag)
			mark(y, x)
		else {
			game.reveal(y, x)
			if (game.grid(y)(x) == Empty(0) || game.hasEnded)
				refreshAll()
			else
				refresh(y, x)
			updateStatus()
		}
	def mark (y:Int, x:Int) = {
		game.mark(y, x)
		refresh(y, x)
		updateStatus()
	}
	def cancelLastRevealed () = {
		game.cancelLastRevealed()
		refresh(game.lastY, game.lastX)
		updateStatus()
	}
	def defuse () = {
		game.defuse()
		refreshAll()
		timer.stop()
		updateStatus()
	}
	def restart () = {
		game.restart()
		refreshAll()
		initTimer()
		updateStatus()
	}
	/* Reset the timer and start it. */
	def initTimer () = {
		timer.stop()
		timerLabel.text = "00:00"
		t0 = System.currentTimeMillis
		timer.start()
	}
	/* Refresh the status information displayed. */
	def updateStatus () : Unit = {
		flagsCounter.text = "%d flags" format game.flags
		minesCounter.text = "%d mines" format game.mines
		multiButton.icon = game.status match {
		  case Status.Playing => Resource.imgStatusPlaying
		  case Status.Mercy   => Resource.imgStatusMercy
		  case Status.Lost    => Resource.imgStatusLost
		  case Status.Won     => Resource.imgStatusWon
		}
		restartButton.enabled = !game.hasEnded
	}
	/* Perform the “multi”-button action according to the current game status. */
	def multiButtonDo () =
		game.status match {
		  case Status.Playing => defuse()
		  case Status.Mercy   => cancelLastRevealed()
		  case Status.Lost    => publish(NewGameRequest())
		  case Status.Won     => publish(NewGameRequest())
		}
}
