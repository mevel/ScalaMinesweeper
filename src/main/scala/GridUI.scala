/*
 * A ‘GridUI’ Swing component than can be used to display grids.
 */
package GridUI

import swing._
import javax.swing.border._
import java.awt.Color

private object Resource
{
	val normalBorder   = new LineBorder(Color.gray,  2)
	val selectedBorder = new LineBorder(Color.black, 2)
}

/* ‘CellUI’ is a wrapper around the cell component. Il permits retrieving cell
 * coordinates in reactions and in-place modification of cell contents. */
case class CellUI(y:Int, x:Int) extends BoxPanel(Orientation.NoOrientation)
{
	private var c : Component = new Label("")
	def set (c2:Component) = {
		contents -= c
		c = c2
		contents += c
		revalidate()
		repaint()
	}
	def selected (b:Boolean) =
		border = if (b) Resource.selectedBorder else Resource.normalBorder
	selected(false)
}

/* To use the abstract class ‘GridUI’, one must inherit from it and provide the
 * ‘getCellComponent’ method which, when given coordinates, produces a component
 * that actually represent the associated cell.
 * ‘GridUI’ is in fact a BorderPanel so that additional elements can be put
 * around the grid.
 * Since the ‘GridUI’ listens to its keyboard and to its ‘CellUI’ mouse events,
 * those events can be exploited by the concrete implementation.
 * That implementation can also make use of the coordinates of the selected cell
 * to adapt its actions.
 */
abstract class GridUI(val h:Int, val w:Int) extends BorderPanel
{
	/* the grid component itself: */
	val gridUI = new GridPanel(h, w)
	/* a matrix intended for direct access to CellUI components: */
	private val grid = Array.ofDim[CellUI](h, w)
	/* coordinates of the selected cell: */
	protected var (ysel,xsel) = (0,0)
	layout(gridUI) = BorderPanel.Position.Center
	/* Create the ‘CellUI’ wrappers and listen to their mouse events. */
	for (y <- 0 until h; x <- 0 until w) {
		grid(y)(x) = CellUI(y, x)
		listenTo(grid(y)(x).mouse.clicks)
		gridUI.contents += grid(y)(x)
	}
	refreshAll()
	/* A cell can be selected with keyboard arrow keys or left-clicking. */
	select(0, 0)
	gridUI.focusable = true
	listenTo(gridUI.keys)
	reactions += {
	  case event.KeyPressed(`gridUI`, event.Key.Left,  _, _) =>
		move_selection(dx = -1)
	  case event.KeyPressed(`gridUI`, event.Key.Right, _, _) =>
		move_selection(dx = +1)
	  case event.KeyPressed(`gridUI`, event.Key.Up,    _, _) =>
		move_selection(dy = -1)
	  case event.KeyPressed(`gridUI`, event.Key.Down,  _, _) =>
		move_selection(dy = +1)
	  case e @ event.MouseClicked(CellUI(y,x), _, _, _, _) if e.peer.getButton == 1 =>
		select(y, x)
	}
	def selection =
		(ysel,xsel)
	def select (y:Int, x:Int) = {
		grid(ysel)(xsel).selected(false)
		grid(y)(x).selected(true)
		ysel = y
		xsel = x
	}
	def move_selection (dy:Int = 0, dx:Int = 0) =
		select(y = (h+ysel+dy) % h, x = (w+xsel+dx) % w)
	def getCellComponent (y:Int, x:Int) : Component
	def refresh (y:Int, x:Int) =
		grid(y)(x).set(getCellComponent(y,x))
	def refreshAll () =
		for (y <- 0 until h; x <- 0 until w)
			refresh(y, x)
}
